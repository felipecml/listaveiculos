/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package veiculoAbstract;

/**
 *
 * @author Felipe de Brito Lira
 */
public abstract class AbstractVeiculo {
    
    private String identificacao;
    private double valor;
    
    public AbstractVeiculo(){}
    
    public AbstractVeiculo(String id, double valor){
        this.identificacao = id;
        this.valor = valor;
    }

    public String getIdentificacao() {
        return identificacao;
    }

    public void setIdentificacao(String identificacao) {
        this.identificacao = identificacao;
    }

    public double getValor() {
        return valor;
    }

    public void setValor(double valor) {
        this.valor = valor;
    }
            
    public abstract double calcularDiaria();
        
}
