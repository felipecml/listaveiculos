/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package colecoes;

import interfaces.Comparador;

//import beans.Aviao;
//import beans.Carro;
//import beans.Helicoptero;
//import beans.Moto;
//import beans.Onibus;

/**
 *
 * @author Felipe de Brito Lira
 */
public class ListaVeiculos<T> {
    
    private T[] lista;
    private int quantidade;
    private int bloco;
    
    public ListaVeiculos(){
        this.lista = (T[]) new Object[1000];
        this.quantidade = 0;
        this.bloco = 20;
    }
    
    public T[] getLista(){
        return lista;
    }
    
    public int getQuantidade(){
        return quantidade;
    }
           
    public int getBloco(){
        return bloco;
    }
    
    public boolean add(T object){
        if( isFull() ) return false;
        this.lista[quantidade++] = object;
        return true;
    }
    
    public T get(int index){
        return this.lista[index];
    }
    
    public boolean isFull(){
        return this.quantidade == this.lista.length;
    }
    
    public int size(){
        return this.quantidade;
    }
    
    public T[] getList(){
        T[] nova = (T[])new Object[ this.quantidade ];
        for( int i = 0 ; i < this.quantidade; i++ ){
            nova[i] = lista[i];
        }
        return nova;
    }
    
    public T[] getList( Class classe ){
        int size = count( classe );
        int index = 0;
        
        T[] nova = (T[]) new Object[ size ];
        
        for( int i = 0; i < this.quantidade; i++){
            if( lista[i].getClass().getName().equals( classe.getName() ) ){
                nova[index++] = lista[i];
            }
        }
        return nova;
    }
    
    public int getQuantidadeCadastrada(){
        return size();
    }
    
    public boolean sort(){
        return ordenarLista( lista );
    }
    
    public T[] getListaEspecifica( Class classes ){
        T[] list = getList(classes);
        ordenarLista(list);
        return list;
    }
    
    private int count( T[] objetos ){
        int cont = 0;
        for( int i = 0; i < objetos.length; i++){
            if( objetos[i] != null ) cont++;
        }
        return cont;
    }
    
    private int count( Class classe ){
        int cont = 0;
        for(int i = 0; i < this.quantidade; i++){
            if( lista[i].getClass().getName().equals( classe.getName() ) ){
                cont++;
            }
        }
        return cont;
    }
    
    private boolean existe( T[] objetos ){  
        if( this.quantidade > 0 ){  
            int size = count(objetos);
            for(int i = 0; i < size; i++){
                Object o = objetos[i];
                int cont = 0;
                Class<?> interfaces[] = objetos.getClass().getInterfaces();
                for( int j = 0; j < interfaces.length; j++){
                    if( interfaces[j] == Comparador.class ){
                        cont++;
                    }
                }
                if( cont == 0){
                    return false;
                }
            }
        }
        return true;
    }
    
    private boolean ordenarLista( T[] objetos ){
        if( objetos.length > 0 && existe(objetos) ){
            int min;
            int size = count(objetos);
            for(int i = 0; i <  size - 1; i++){
                min = i;
                for(int j = i + 1; j < size; j++ ){
                    if( ( ((Comparador)objetos[j]).comparar( objetos[min] ) < 0 )){
                        min = j;
                    }
                }
                T object = objetos[i];
                objetos[i] = objetos[min];
                objetos[min] = object;
            }
            return true;
        }
        return false;
    }
    
//    public T[] getListCarro(){
//        return getList( Carro.class );
//    }
//    
//    public T[] getListAviao(){
//        return getList( Aviao.class );
//    }
//    
//    public T[] getListHelicoptero(){
//        return getList( Helicoptero.class );
//    }
//    
//    public T[] getListMoto(){
//        return getList( Moto.class );
//    }
//    
//    public T[] getListOnibus(){
//        return getList( Onibus.class );
//    }
//    
//    public boolean sortCarro( Class classes ){
//        return ordenarLista( getList(classes) );
//    }
//    
//    public boolean sortAviao( Class classes ){
//        return ordenarLista( getList(classes) );
//    }
//    
//    public boolean sortHelicoptero( Class classes ){
//        return ordenarLista( getList(classes) );
//    }
//    
//    public boolean sortMoto( Class classes ){
//        return ordenarLista( getList(classes) );
//    }
//    
//    public boolean sortOnibus( Class classes ){
//        return ordenarLista( getList(classes) );
//    }
    
}