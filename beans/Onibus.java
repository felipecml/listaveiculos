/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interfaces.Comparador;
import veiculoAbstract.AbstractVeiculo;

/**
 *
 * @author Felipe
 */
public class Onibus extends AbstractVeiculo implements Comparador<Onibus>{

    private String motor;
    private String placa;
    private int quantidadeDePassageiros;
    
    public Onibus(){
        super();
    }
    
    public Onibus(String motor, String placa, int quantidadeDePassageiros, String id, double valor){
        super(id,valor);
        this.motor = motor;
        this.placa = placa;
        this.quantidadeDePassageiros = quantidadeDePassageiros;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public int getQuantidadeDePassageiros() {
        return quantidadeDePassageiros;
    }

    public void setQuantidadeDePassageiros(int quantidadeDePassageiros) {
        this.quantidadeDePassageiros = quantidadeDePassageiros;
    }
    
    @Override
    public double calcularDiaria() {
        return ((this.getValor() * 0.04) + 100) * this.quantidadeDePassageiros;
    }

    @Override
    public int comparar(Onibus object) {
        return this.getIdentificacao().compareToIgnoreCase( object.getIdentificacao() );
    }
    
}
