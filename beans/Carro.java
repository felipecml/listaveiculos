/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interfaces.Comparador;
import veiculoAbstract.AbstractVeiculo;

/**
 *
 * @author Felipe
 */
public class Carro extends AbstractVeiculo implements Comparador<Carro>{
    
    private String autonomia;
    private String motor;
    private String placa;

    public Carro(){
        super();
    }
    
    public Carro(String autonomia, String motor, String placa, String id, double valor){
        super(id,valor);
        this.autonomia = autonomia;
        this.motor = motor;
        this.placa = placa;
    }

    public String getAutonomia() {
        return autonomia;
    }

    public void setAutonomia(String autonomia) {
        this.autonomia = autonomia;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }
    
    @Override
    public double calcularDiaria() {
        return this.getValor() * 0.02;
    }

    @Override
    public int comparar(Carro object) {
        return this.getIdentificacao().compareToIgnoreCase( object.getIdentificacao() );
    }
    
}
