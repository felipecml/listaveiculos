/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interfaces.Comparador;
import veiculoAbstract.AbstractVeiculo;

/**
 *
 * @author Felipe
 */
public class Aviao extends AbstractVeiculo implements Comparador<Aviao>{
    
    private int tempoDeVoo;
    private double velocidadeDeCruzeiro;
    
    public Aviao(){
        super();
    }
    
    public Aviao(int tempoDeVoo, double velocidade, String id, double valor){
        super(id,valor);
        this.tempoDeVoo = tempoDeVoo;
        this.velocidadeDeCruzeiro = velocidade;
    }

    public int getTempoDeVoo(){
        return tempoDeVoo;
    }
    
    public void setTempoDeVoo(int tempo){
        this.tempoDeVoo = tempo;
    }
    
    public double getVelocidadeDeCruzeiro(){
        return velocidadeDeCruzeiro;
    }
    
    public void setVelocidadeDeCruzeiro(double velocidade){
        this.velocidadeDeCruzeiro = velocidade;
    }
    
    @Override
    public double calcularDiaria() {
        return ((this.getValor() * 0.2) + 1000 ) * this.velocidadeDeCruzeiro;
    }

    @Override
    public int comparar(Aviao object) {
        return this.getIdentificacao().compareToIgnoreCase( object.getIdentificacao() );
    }
    
}
