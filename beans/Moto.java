/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interfaces.Comparador;
import veiculoAbstract.AbstractVeiculo;

/**
 *
 * @author Felipe
 */
public class Moto extends AbstractVeiculo implements Comparador<Moto>{

    private final int ELETRICO = 20;
    private final int MANUAL = 20;
    
    private String motor;
    private String placa;
    private String sistemaDePartida;
    
    public Moto(){
        super();
    }
    
    public Moto(String motor, String placa, String sistemaDePartida, String id, double valor){
        super(id,valor);
        this.motor = motor;
        this.placa = placa;
        this.sistemaDePartida = sistemaDePartida;
    }

    public String getMotor() {
        return motor;
    }

    public void setMotor(String motor) {
        this.motor = motor;
    }

    public String getPlaca() {
        return placa;
    }

    public void setPlaca(String placa) {
        this.placa = placa;
    }

    public String getSistemaDePartida() {
        return sistemaDePartida;
    }

    public void setSistemaDePartidaEletrico() {
        this.sistemaDePartida = "ELÉTRICO";
    }
    
    public void setSistemaDePartidaManual(){
        this.sistemaDePartida = "MANUAL";
    }
    
    @Override
    public double calcularDiaria() {
        double valor = this.getValor() * 0.02 ;
        if ( this.sistemaDePartida.equalsIgnoreCase("elétrico") ){
            return valor + ELETRICO;
        }else{
            return valor + MANUAL;
        }
    }

    @Override
    public int comparar(Moto object) {
        return this.getIdentificacao().compareToIgnoreCase( object.getIdentificacao() );
    }
    
}
