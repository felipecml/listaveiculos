/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package beans;

import interfaces.Comparador;
import veiculoAbstract.AbstractVeiculo;

/**
 *
 * @author Felipe
 */
public class Helicoptero extends AbstractVeiculo implements Comparador<Helicoptero>{
    
    private int quantidadeDeRotores;
    private int tempoDeVoo;

    public Helicoptero(){
        super();
    }
    
    public Helicoptero(int quantidadeDeRotores, int tempoDeVoo, String id, double valor){
        super(id,valor);
        this.quantidadeDeRotores = quantidadeDeRotores;
        this.tempoDeVoo = tempoDeVoo;
    }

    public int getQuantidadeDeRotores() {
        return quantidadeDeRotores;
    }

    public void setQuantidadeDeRotores(int quantidadeDeRotores) {
        this.quantidadeDeRotores = quantidadeDeRotores;
    }

    public int getTempoDeVoo() {
        return tempoDeVoo;
    }

    public void setTempoDeVoo(int tempoDeVoo) {
        this.tempoDeVoo = tempoDeVoo;
    }
    
    @Override
    public double calcularDiaria() {
        return (this.getValor() * 0.1) * this.quantidadeDeRotores;
    }

    @Override
    public int comparar(Helicoptero object) {
        return this.getIdentificacao().compareToIgnoreCase( object.getIdentificacao() );
    }
    
}
