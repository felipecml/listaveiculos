/*
 * To change this template, choose Tools | Templates
 * and open the template in the editor.
 */
package interfaces;

/**
 *
 * @author Felipe
 */
public interface Comparador<T> {
    
    public int comparar(T object);
    
}
